import React, { Component,Fragment } from 'react';
import './App.css';
import {
  Route,
  withRouter,
  Switch
} from 'react-router-dom';

import { getCurrentUser } from '../util/APIUtils';
import { ACCESS_TOKEN } from '../constants';
import LoadingIndicator from '../common/LoadingIndicator';
import Login from '../pages/Login';
import Signup from '../pages/Signup';
import Publications from '../pages/Publications';
import PrivateRoute from '../common/PrivateRoute';
import PublicRoute from '../common/PrivateRoute';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentUser: null,
      isAuthenticated: false,
      isLoading: false,
    }
    this.handleLogout = this.handleLogout.bind(this);
    this.loadCurrentUser = this.loadCurrentUser.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    const  socket = null;
    const  stompClient = null;
  }
  
  

  loadCurrentUser() {
    this.setState({
      isLoading: true
    });
    getCurrentUser()
    .then(response => {

      this.setState({
        currentUser: response,
        isAuthenticated: true,
        isLoading: false
      });

    

    }).catch(error => {
      this.setState({
        isLoading: false
      });  
    });
  }

  


  componentWillMount() {
    console.log("ok....");
    if(!this.state.isAuthenticated) {
     this.loadCurrentUser();
    }
    
  }

  handleLogout() {
    localStorage.removeItem(ACCESS_TOKEN);
    
    this.setState({
      currentUser: null,
      isAuthenticated: false
    });

    this.props.history.push("/login");
    
  }

  handleLogin() {
    this.loadCurrentUser();
    this.props.history.push("/");
  }


  render() {
    if(this.state.isLoading) {
      return <LoadingIndicator />
    }

    return (
        <Switch>
        <Fragment>
      
        {/* <PublicRoute authenticated={this.state.isAuthenticated} restricted={true} component={Login } path="/login" exact /> */}

              <Route path="/login" render={(props) => <Login onLogin={this.handleLogin} isAuthenticated={this.state.isAuthenticated} {...props} />}></Route>
              <Route path="/signup" component={Signup}></Route>
          
              <PrivateRoute exact
                      authenticated={this.state.isAuthenticated} path="/" 
                      component={Publications}
                      data={{
                        currentUser:this.state.currentUser,
                        handleLogout:this.handleLogout
                        
                      }}
               />
      


        </Fragment>
        </Switch>
         
      
    );
  }
}

export default withRouter(App);
