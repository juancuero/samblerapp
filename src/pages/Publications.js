import React, { Component } from 'react'
import { Link } from "react-router-dom";
import AppSideBar from '../common/AppSideBar';
import AppNav from '../common/AppNav';
import LoadingIndicator from '../common/LoadingIndicator';
import Publication from '../common/Publication';
import { PUBLICATION_LIST_SIZE } from '../constants';
import { getAllPublications} from '../util/APIUtils';

class Publications extends Component {

    state = {
        publications: [],
        page: 0,
        size: 10,
        totalElements: 0,
        totalPages: 0,
        last: true,
        isLoading: false
    }

    loadPublicationsList(page = 0, size = PUBLICATION_LIST_SIZE){
        let promise;
        promise = getAllPublications(page, size);

        if(!promise) {
            return;
        }

        this.setState({
            isLoading: true
        });

        promise            
        .then(response => {
            const publications = this.state.publications.slice();

            this.setState({
                publications: publications.concat(response.content),
                page: response.page,
                size: response.size,
                totalElements: response.totalElements,
                totalPages: response.totalPages,
                last: response.last,
                isLoading: false
            }, () => console.log(this.state.publications))
        }).catch(error => {
            this.setState({
                isLoading: false
            })
        }); 

    }


    handleLoadMore = () => {    
        console.log("handleScroll..");
        if (window.innerHeight + document.documentElement.scrollTop >= document.documentElement.offsetHeight -50 && !this.state.last) {
            console.log("llamar..");
            console.log("window.innerHeight: "+window.innerHeight);
            console.log("document.documentElement.scrollTop: "+document.documentElement.scrollTop);
            console.log("Suma: "+(window.innerHeight + document.documentElement.scrollTop));
            console.log("document.documentElement.offsetHeight: "+document.documentElement.offsetHeight);
            this.loadPublicationsList(this.state.page + 1);
        }
    }

    componentDidMount() {
        console.log("ok Publications....");
        this.loadPublicationsList();
        window.onscroll = () => this.handleLoadMore();  
      }

    updatePublication = (publicationUpdate) => { 
        console.log( 'Ok updatePublication...');
        console.log(publicationUpdate);

        const newPublications = this.state.publications.map(publication => {
            if(publication.id === publicationUpdate.id){
                publication = publicationUpdate;
            }
            return publication;
        })
        this.setState({
            publications: newPublications
        })

    }

    render() {

        const publicationViews = [];
        this.state.publications.forEach((publication, publicationIndex) => {
            publicationViews.push(<Publication 
                key={publication.id} 
                publication={publication}
                updatePublication = {this.updatePublication}
                 />)            
        });



        return (

            <div id="wrapper">
                <AppSideBar/>
                <div id="content-wrapper" className="d-flex flex-column">
                    <div id="content">
                            <AppNav currentUser={this.props.currentUser} handleLogout={this.props.handleLogout}   />
                            <div className="container-fluid">
                                <div className="row">

                                    {publicationViews}
                                 

                                    {
                                        !this.state.isLoading && this.state.publications.length === 0 ? (
                                            <div className="no-polls-found">
                                                <span>No Publications Found.</span>
                                            </div>    
                                        ): null
                                    }  
                                    {
                                        !this.state.isLoading && this.state.last ? (
                                            <div className=""> 
                                            
                                                <h3>NO MORE DATA</h3>
                                            
                                            </div>): null
                                    }              
                                    {
                                        this.state.isLoading ? 
                                        <LoadingIndicator />: null                     
                                    }
                                     
                                </div>


                            </div>
                    </div>
                    <footer className="sticky-footer bg-white">
                    <div className="container my-auto">
                        <div className="copyright text-center my-auto">
                        <span>Copyright &copy; Sambler 2019</span>
                        </div>
                    </div>
                    </footer>
                </div>
            </div>

        )
    }
}

export default Publications;
