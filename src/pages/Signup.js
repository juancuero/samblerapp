import React, { Component } from 'react';
import { signup, checkUsernameAvailability, checkEmailAvailability } from '../util/APIUtils';
import { Link } from 'react-router-dom';
import { 
    NAME_MIN_LENGTH, NAME_MAX_LENGTH, 
    USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH,
    EMAIL_MAX_LENGTH,
    PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH
} from '../constants';

class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: {
                value: ''
            },
            username: {
                value: ''
            },
            email: {
                value: ''
            },
            password: {
                value: ''
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.validateUsernameAvailability = this.validateUsernameAvailability.bind(this);
        this.validateEmailAvailability = this.validateEmailAvailability.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);
    }


    handleInputChange(event, validationFun) {
        const target = event.target;
        const inputName = target.name;        
        const inputValue = target.value;

        this.setState({
            [inputName] : {
                value: inputValue,
                ...validationFun(inputValue)
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    
        const signupRequest = {
            name: this.state.name.value,
            email: this.state.email.value,
            username: this.state.username.value,
            password: this.state.password.value
        };
        signup(signupRequest)
        .then(response => {
            console.log( 'Ok register...');
            this.props.history.push("/login");
        }).catch(error => {
            console.log( 'Sorry! Something went wrong. Please try again!');
   
        });
    }


    isFormInvalid() {
      return !(this.state.name.validateStatus === 'success' &&
          this.state.username.validateStatus === 'success' &&
          this.state.email.validateStatus === 'success' &&
          this.state.password.validateStatus === 'success'
      );
    }

    render() {


    const renderNameValidationError = this.state.name.validateStatus === 'success' ? "" : <div className="invalid-feedback" style={{display: 'block'}}>{this.state.name.errorMsg}</div>;
    const renderUsernameValidationError = this.state.username.validateStatus === 'success' ? "" : <div className="invalid-feedback" style={{display: 'block'}}>{this.state.username.errorMsg}</div>;
    const renderEmailValidationError = this.state.email.validateStatus === 'success' ? "" : <div className="invalid-feedback" style={{display: 'block'}}>{this.state.email.errorMsg}</div>;
    const renderPasswordValidationError = this.state.password.validateStatus === 'success' ? "" : <div className="invalid-feedback" style={{display: 'block'}}>{this.state.password.errorMsg}</div>;



        return (
            <div className="container">

    <div className="card o-hidden border-0 shadow-lg my-5">
      <div className="card-body p-0">
        <div className="row">
          <div className="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div className="col-lg-7">
            <div className="p-5">
              <div className="text-center">
                <h1 className="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
              <form className="user" onSubmit={this.handleSubmit}>
                <div className="form-group row">
                  <div className="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" className="form-control form-control-user" 
                        name="name"
                        value={this.state.name.value}
                        onChange={(event) => this.handleInputChange(event, this.validateName)}
                        id="exampleFirstName" placeholder="Full Name"
                    />

                    {renderNameValidationError}

                    
                  </div>
                  <div className="col-sm-6">
                    <input type="text" className="form-control form-control-user"
                        name="username"
                        value={this.state.username.value}
                        onBlur={this.validateUsernameAvailability}
                        onChange={(event) => this.handleInputChange(event, this.validateUsername)}
                        id="exampleLastName" placeholder="Username"
                    />

                    {renderUsernameValidationError}

                  </div>
                </div>
                <div className="form-group">
                  <input type="email" className="form-control form-control-user" 
                        name="email"
                        value={this.state.email.value}
                        onChange={(event) => this.handleInputChange(event, this.validateEmail)}
                        onBlur={this.validateEmailAvailability}
                        id="exampleInputEmail" placeholder="Email Address"
                  />

                      {renderEmailValidationError}

                </div>
                <div className="form-group row">
                  <div className="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" className="form-control form-control-user"
                        name="password"
                        id="exampleInputPassword" placeholder="Password"
                        placeholder="Password" 
                        value={this.state.password.value} 
                        onChange={(event) => this.handleInputChange(event, this.validatePassword)}
                    />

                      {renderPasswordValidationError}

                  </div>
                  <div className="col-sm-6">
                    <input type="password" className="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat Password"/>
                  </div>
                </div>
                <button className="btn btn-primary btn-user btn-block" type="submit" disabled={this.isFormInvalid()}>
                  Register Account
                </button>
              
              </form>
              <hr/>
              <div className="text-center">
                <a className="small" href="forgot-password.html">Forgot Password?</a>
              </div>
              <div className="text-center">
                <Link className="small" to="/login">Already have an account? Login!</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
        )
    }

    validateName = (name) => {
      if(name.length < NAME_MIN_LENGTH) {
          return {
              validateStatus: 'error',
              errorMsg: `Name is too short (Minimum ${NAME_MIN_LENGTH} characters.)`
          }
      } else if (name.length > NAME_MAX_LENGTH) {
          return {
              validationStatus: 'error',
              errorMsg: `Name is too long (Maximum ${NAME_MAX_LENGTH} characters.)`
          }
      } else {
          return {
              validateStatus: 'success',
              errorMsg: null,
            };            
      }
  }

  validateUsername = (username) => {
    if(!username) {
      return {
          validateStatus: 'error',
          errorMsg: 'Username may not be empty'                
      }
    }else if(username.length < USERNAME_MIN_LENGTH) {
        return {
            validateStatus: 'error',
            errorMsg: `Username is too short (Minimum ${USERNAME_MIN_LENGTH} needed.)`
        }
    } else if (username.length > USERNAME_MAX_LENGTH) {
        return {
            validationStatus: 'error',
            errorMsg: `Username is too long (Maximum ${USERNAME_MAX_LENGTH} allowed.)`
        }
    } else {
        return {
            validateStatus: null,
            errorMsg: null
        }
    }
  }

  validateEmail = (email) => {
    if(!email) {
        return {
            validateStatus: 'error',
            errorMsg: 'Email may not be empty'                
        }
    }

    const EMAIL_REGEX = RegExp('[^@ ]+@[^@ ]+\\.[^@ ]+');
    if(!EMAIL_REGEX.test(email)) {
        return {
            validateStatus: 'error',
            errorMsg: 'Email not valid'
        }
    }

    if(email.length > EMAIL_MAX_LENGTH) {
        return {
            validateStatus: 'error',
            errorMsg: `Email is too long (Maximum ${EMAIL_MAX_LENGTH} characters allowed)`
        }
    }

    return {
        validateStatus: null,
        errorMsg: null
    }
}

  validateUsernameAvailability() {
    // First check for client side errors in username
    const usernameValue = this.state.username.value;
    const usernameValidation = this.validateUsername(usernameValue);

      if(usernameValidation.validateStatus === 'error') {
          this.setState({
              username: {
                  value: usernameValue,
                  ...usernameValidation
              }
          });
          return;
      }

      this.setState({
          username: {
              value: usernameValue,
              validateStatus: 'validating',
              errorMsg: null
          }
      });

      checkUsernameAvailability(usernameValue)
      .then(response => {
          if(response.available) {
              this.setState({
                  username: {
                      value: usernameValue,
                      validateStatus: 'success',
                      errorMsg: null
                  }
              });
          } else {
              this.setState({
                  username: {
                      value: usernameValue,
                      validateStatus: 'error',
                      errorMsg: 'This username is already taken'
                  }
              });
          }
      }).catch(error => {
          // Marking validateStatus as success, Form will be recchecked at server
          this.setState({
              username: {
                  value: usernameValue,
                  validateStatus: 'success',
                  errorMsg: null
              }
          });
      });
  }

  validateEmailAvailability() {
    // First check for client side errors in email
    const emailValue = this.state.email.value;
    const emailValidation = this.validateEmail(emailValue);

    if(emailValidation.validateStatus === 'error') {
        this.setState({
            email: {
                value: emailValue,
                ...emailValidation
            }
        });    
        return;
    }

    this.setState({
        email: {
            value: emailValue,
            validateStatus: 'validating',
            errorMsg: null
        }
    });

    checkEmailAvailability(emailValue)
    .then(response => {
        if(response.available) {
            this.setState({
                email: {
                    value: emailValue,
                    validateStatus: 'success',
                    errorMsg: null
                }
            });
        } else {
            this.setState({
                email: {
                    value: emailValue,
                    validateStatus: 'error',
                    errorMsg: 'This Email is already registered'
                }
            });
        }
    }).catch(error => {
        // Marking validateStatus as success, Form will be recchecked at server
        this.setState({
            email: {
                value: emailValue,
                validateStatus: 'success',
                errorMsg: null
            }
        });
    });
  }

  validatePassword = (password) => {
    if(password.length < PASSWORD_MIN_LENGTH) {
        return {
            validateStatus: 'error',
            errorMsg: `Password is too short (Minimum ${PASSWORD_MIN_LENGTH} characters.)`
        }
    } else if (password.length > PASSWORD_MAX_LENGTH) {
        return {
            validationStatus: 'error',
            errorMsg: `Password is too long (Maximum ${PASSWORD_MAX_LENGTH} characters.)`
        }
    } else {
        return {
            validateStatus: 'success',
            errorMsg: null,
        };            
    }
}

}

export default Signup;
