import React, { Component } from 'react'
import {
  Link
} from 'react-router-dom';
import { login } from '../util/APIUtils';
import { ACCESS_TOKEN } from '../constants';

export default class Login extends Component {

    state = {
        usernameOrEmail: '',
        password: ''
    }


    handleSubmit = e => {
        e.preventDefault();
        console.log(this.state);
        console.log(this.props);
        login(this.state)
                .then(response => {
                    localStorage.setItem(ACCESS_TOKEN, response.accessToken);
                    this.props.onLogin();
                }).catch(error => {
                    if(error.status === 401) {
                        
                            console.log('Your Username or Password is incorrect. Please try again!');
                                           
                    } else {
                            
                        console.log('Sorry! Something went wrong. Please try again!');
                                                                 
                    }
                });
    }

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    componentWillMount() {
      if(this.props.isAuthenticated){
        this.props.history.push("/publications");
      }
    }
    render() {


        return (
            <div className="container">

    
    <div className="row justify-content-center">

      <div className="col-xl-10 col-lg-12 col-md-9">

        <div className="card o-hidden border-0 shadow-lg my-5">
          <div className="card-body p-0">
            
            <div className="row">
              <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div className="col-lg-6">
                <div className="p-5">
                  <div className="text-center">
                    <h1 className="h4 text-gray-900 mb-4">Welcome Back!</h1>
                  </div>
                  <form onSubmit={this.handleSubmit} className="user" >
                    <div className="form-group">
                      <input type="email" className="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address Or Username" name="usernameOrEmail" onChange={this.onChange} 
                    value={this.state.usernameOrEmail} />
                    </div>
                    <div className="form-group">
                      <input type="password" className="form-control form-control-user" id="exampleInputPassword" placeholder="Password" name="password" onChange={this.onChange} 
                    value={this.state.password}/>
                    </div>
                    <div className="form-group">
                      <div className="custom-control custom-checkbox small">
                        <input type="checkbox" className="custom-control-input" id="customCheck"/>
                        <label className="custom-control-label" htmlFor="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <button type="submit" className="btn btn-primary btn-user btn-block">
                      Login
                    </button>
                    <hr/>
                  </form>
                  <hr/>
                  <div className="text-center">
                    <a className="small" href="forgot-password.html">Forgot Password?</a>
                  </div>
                  <div className="text-center">
                  <Link className="small" to="/signup">Create an Account! </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
        )
    }
}
