import React from 'react';
import {
    Route,
    Redirect
  } from "react-router-dom";

const PublicRoute = ({component: Component, authenticated, restricted, ...rest}) => {

    console.log("authenticated: "+authenticated);
    console.log("restricted: "+restricted);

    return (
        // restricted = false meaning public route
        // restricted = true meaning restricted route
        <Route {...rest} render={props => (
            authenticated && restricted ?
                <Redirect to="/" />
            : <Component {...props} />
        )} />
    );
};

export default PublicRoute;