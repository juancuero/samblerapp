import React from 'react';
import {
    Route,
    Redirect
  } from "react-router-dom";
  
  
const PrivateRoute = ({ component: Component, authenticated,data, ...rest }) => {
  console.log("llega -> "+authenticated)
        return (
          <Route
              {...rest}
              render={props =>
                authenticated ? (
                  <Component {...rest} {...props} {...data}  />
                ) : (
                  <Redirect
                    to={{
                      pathname: '/login',
                      state: { from: props.location }
                    }}
                  />
                )
              }
            />
        );
};

  
export default PrivateRoute