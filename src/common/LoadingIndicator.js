import React from 'react';
import './css/LoandingIndicator.css';

export default function LoadingIndicator() {
    const style = { position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)" };

    return (
        <div className="lds-roller" style={style}><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    );
}