import React, { Component } from 'react';
import * as SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBell, faUser, faSignOutAlt, faCogs, faBars } from '@fortawesome/free-solid-svg-icons'
import $ from 'jquery';



export default class AppNav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      countNotify: 0
    }
    const  socket = null;
    const  stompClient = null;
  }

  

  componentDidMount() {
    console.log("ok AppNav....");

    this.LoadScript();

    this.socket = new SockJS('http://127.0.0.1:8080/gs-guide-websocket');
    var stompClient = Stomp.over(this.socket);
    stompClient.connect({
           "user" : this.props.currentUser.username
    }

    , frame =>  {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/reply', data => {

          console.log("la info");
          console.log(data.body);
          console.log(this.state.countNotify);
          this.setState({
            countNotify: data.body
          });

          this.audio = new Audio('../assets/siren-notify.wav')
          var playPromise = this.audio.play();
      if (playPromise !== undefined) {
        playPromise
          .then(_ => {
            // Automatic playback started!
            // Show playing UI.
            console.log("audio played auto");

          })
          .catch(error => {
            // Auto-play was prevented
            // Show paused UI.
            console.log("playback prevented");
          });
      }

         });
    });

    this.stompClient = stompClient;
  }




  handleLogout = () => {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
    this.props.handleLogout();

  }

  LoadScript(){
        // Toggle the side navigation
        $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
          $("body").toggleClass("sidebar-toggled");
          $(".sidebar").toggleClass("toggled");
          if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
          };
        });
      
        // Close any open menu accordions when window is resized below 768px
        $(window).resize(function() {
          if ($(window).width() < 768) {
            $('.sidebar .collapse').collapse('hide');
          };
        });
      
        // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
        $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
          if ($(window).width() > 768) {
            var e0 = e.originalEvent,
              delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
          }
        });
      
        // Scroll to top button appear
        $(document).on('scroll', function() {
          var scrollDistance = $(this).scrollTop();
          if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
          } else {
            $('.scroll-to-top').fadeOut();
          }
        });
      
        // Smooth scrolling using jQuery easing
        $(document).on('click', 'a.scroll-to-top', function(e) {
          var $anchor = $(this);
          $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
          }, 1000, 'easeInOutExpo');
          e.preventDefault();
        });    
  }

    render() {

      const renderCountNotify = this.state.countNotify === 0 ? "" :  <span className="badge badge-danger badge-counter">{this.state.countNotify}+</span>;

        return (
            <div>
                  <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            
                  <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
       
            <FontAwesomeIcon icon={faBars} />
              </button>

            <ul className="navbar-nav ml-auto">

              <li className="nav-item dropdown no-arrow mx-1">
                <a className="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <FontAwesomeIcon icon={faBell} />
                    {renderCountNotify}
                </a>
                
                <div className="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                  <h6 className="dropdown-header">
                    Alerts Center
                  </h6>
                  <a className="dropdown-item d-flex align-items-center" href="#">
                    <div className="mr-3">
                      <div className="icon-circle bg-primary">
                        <i className="fas fa-file-alt text-white"></i>
                      </div>
                    </div>
                    <div>
                      <div className="small text-gray-500">December 12, 2019</div>
                      <span className="font-weight-bold">A new monthly report is ready to download!</span>
                    </div>
                  </a>
                  <a className="dropdown-item d-flex align-items-center" href="#">
                    <div className="mr-3">
                      <div className="icon-circle bg-success">
                        <i className="fas fa-donate text-white"></i>
                      </div>
                    </div>
                    <div>
                      <div className="small text-gray-500">December 7, 2019</div>
                      $290.29 has been deposited into your account!
                    </div>
                  </a>
                  <a className="dropdown-item d-flex align-items-center" href="#">
                    <div className="mr-3">
                      <div className="icon-circle bg-warning">
                        <i className="fas fa-exclamation-triangle text-white"></i>
                      </div>
                    </div>
                    <div>
                      <div className="small text-gray-500">December 2, 2019</div>
                      Spending Alert: We've noticed unusually high spending for your account.
                    </div>
                  </a>
                  <a className="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                </div>
              </li>

              
              <div className="topbar-divider d-none d-sm-block"></div>

              
              <li className="nav-item dropdown no-arrow">
                <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span className="mr-2 d-none d-lg-inline text-gray-600 small">{this.props.currentUser.username}</span>
                  <img className="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"/>
                </a>
                
                <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  <a className="dropdown-item" href="#">
                    <FontAwesomeIcon icon={faUser} className="mr-2 text-gray-400" />
                    Profile
                  </a>
                  <a className="dropdown-item" href="#">
                    <FontAwesomeIcon icon={faCogs} className="mr-2 text-gray-400" />
                    Settings
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" onClick={this.handleLogout} >
                    <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    <FontAwesomeIcon icon={faSignOutAlt} className="mr-2 text-gray-400" />
                    Logout
                  </a>
                </div>
              </li>
            </ul>
          </nav>
            </div>
        )
    }
}
