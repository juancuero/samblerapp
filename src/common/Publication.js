import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faClock, faMapMarkedAlt,faEllipsisV, faHeart, faCommentAlt, faShare, faEye, faMap, faMapMarked } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom';
import { likeOrSharePublication } from '../util/APIUtils';
import LoadingIndicator from './LoadingIndicator';
export default class Publication extends Component {

  state = {
    isLoading: false
  }

  StyleActions(action){
    return {
        color: action ? 'red' : '#b2b2b2',
      }
  }

  handleLikeOrSharePublication = (action) => {
      console.log("handleLike"+this.props.publication.id);
      this.setState({
        isLoading: true
      })
      likeOrSharePublication(this.props.publication.id,action)
        .then(response => {
            console.log( 'Ok like...');
            console.log(response);
            this.props.updatePublication(response);
        }).catch(error => {
            console.log( 'Sorry! Something went wrong. Please try again!');
   
        });
        this.setState({
          isLoading: false
      })
  }

    render() {

      if(this.state.isLoading) {
        return <LoadingIndicator />
      }

        return (
          
          <div className="col-lg-6">
              
            <div className="card shadow mb-4">
              
              <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <div className="row">
                  <div className="col-2">
                    <img className="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"/>   
                  </div>
                  <div className="col-10 post-user">
                    <h6 className="m-0 font-weight-bold text-primary">{this.props.publication.user.username}</h6>
                    <span><FontAwesomeIcon icon={faClock} /> {this.props.publication.createdAt} </span> <br/>
                    <span><FontAwesomeIcon icon={faMapMarkedAlt} /> Colombia - Villavicencio</span> <br/>
                  </div>
                </div>


                <div className="dropdown no-arrow">
                  <a className="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <FontAwesomeIcon icon={faEllipsisV} /> 
                  </a>
                  <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div className="dropdown-header">Dropdown Header:</div>
                    <a className="dropdown-item" href="#">Action</a>
                    <a className="dropdown-item" href="#">Another action</a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
              </div>
              
              <div className="card-body">          
                <h4 className="title-post">{this.props.publication.title}</h4>
                <p>{this.props.publication.description}<a href="nuevo2.html" title="">view more</a></p>
                <ul className="post-tags">
                  <li><a href="#" title="">HTML</a></li>
                  <li><a href="#" title="">PHP</a></li>
                  <li><a href="#" title="">CSS</a></li>
                  <li><a href="#" title="">Javascript</a></li>
                  <li><a href="#" title="">Wordpress</a></li>   
                </ul>
              </div>
              <div className="card-footer links-post mx-auto">
                <ul>
                  <li>
                  <Link onClick={() => this.handleLikeOrSharePublication("like")} className="com" style={this.StyleActions(this.props.publication.liked)}><FontAwesomeIcon icon={faHeart} /> Likes {this.props.publication.cantLikes}</Link>
                  </li>
                  <li>
                  <a href="#" className="com"><FontAwesomeIcon icon={faCommentAlt} />  Comment 8</a>
                  </li>
                  <li>
                  <Link onClick={() => this.handleLikeOrSharePublication("share")} className="com" style={this.StyleActions(this.props.publication.shared)}><FontAwesomeIcon icon={faShare} />  Share {this.props.publication.cantShares}</Link>
                  </li>
                  <li>
                  <a href="#" className="com"  ><FontAwesomeIcon icon={faEye} />  Views 36</a>
                  </li>
                </ul>         
              </div>
            </div>
          </div>                   
        


        )
    }
}
